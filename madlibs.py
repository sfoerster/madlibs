
import os
import sys
import json
import re
import random

class MadlibStory:

    # story data
    data = None
    groups = None

    def __init__(self, filename=None):
    
        if not filename:
            print("No story file given. Using example.")
            filepath=os.path.join(os.path.dirname(__file__), 'stories', 'example.json')
        else:
            print(f"Using {filename}")
            filepath=os.path.join(os.path.dirname(__file__), 'stories', filename)
        
        # read the story data from the file
        with open(filepath, 'r') as f:
            self.data = json.load(f)

        #print(f"Data: {self.data}")
        if self.data:
            self.prompts = self.prepare()
            self.prompt()
            self.replace()
            print(self.data['story'])

    def prompt(self):
        
        # random shuffle of the markers
        random.shuffle(self.prompts)

        # prompt for story inputs
        self.groups = {}
        for p in self.prompts:
            self.groups[p[1]] = {'type': p[2],
                                 'group': p[3],
                                 'value': input(p[0])}

    def replace(self):
        
        # replace markers in story
        for k,v in self.groups.items():
            #print(f"Key: {k} Val: {v}")
            
            # search for matching articles for nouns
            if v.get('type') == 'noun':
                artstring = f"__article{v.get('group')}__"
                if re.search(artstring, self.data.get('story')):
                    
                    # if noun begins with vowel, article is 'an', otherwise 'a'
                    if v.get('value').lower()[0] in ['a','e','i','o','u']:
                        self.data['story'] = self.data['story'].replace(artstring, 'an')
                    else:
                        self.data['story'] = self.data['story'].replace(artstring, 'a')

            # replace with noun or adjective
            self.data['story'] = self.data['story'].replace(k, v.get('value'))

    def prepare(self):

        prompts = []
        
        # find all noun markers in story
        noun_array = re.findall(r"__noun\d+__", self.data.get('story'))

        for n in noun_array:
            num = re.search(r"\d+", n)
            prompts.append(["Noun: ", n, 'noun', num.group()])

        # find all adjective markers in story
        adj_array = re.findall(r"__adjective\d+__", self.data.get('story'))

        for a in adj_array:
            num = re.search(r"\d+", a)
            prompts.append(["Adjective: ", a, 'adj', num.group()])

        return prompts

if __name__ == '__main__':
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = None
    example = MadlibStory(filename=filename)
