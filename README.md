# Madlibs

Create madlib stories

# Table of Contents
[[_TOC_]]

## How to use
```
python3 madlibs.py
```

## Create story templates
See `stories/example.json`

```
python3 madlibs.py example.json
```
